# Python snippets

## Numpy
### Loading data:
```python
import numpy as np

# For .npy
data = np.load("file.npy")

# For any other
data = np.loadtxt("file.txt", delimiter = ";") # file can be whatever, just specify delimiter
```

### Complex numbers:
```python
ComplexNumber = a + 1j*b

Real = np.real(ComplexNumber)
Imag = np.imag(ComplexNumber)
```

## Scipy
### Linear regression:
```python
import scipy as scp

# given x and y arrays
reg = scp.stats.linregress(x, y)

slope = reg.slope # or reg[0]
intersect = reg.intersect # or reg[1]
reg_coeff = reg[2]
```

### Integration:
```python
# For analytic integration
integral = scp.integrate.quad(f, a, b)[0] # "a" and "b" being inferior and superior limit

# For numeric integration
y = np.linspace(a, b, n)

integral = scp.integrate.simpson(y = y, # data to be integrated
                                 dx = (b-a)/n) # step
```

### Differential equations:
```python
# For a problem with these variables: x, v, a (pos, vel, acc)
def edo (y, t):
    pos, vel = y
    acc = {} # expression for acceleration in terms of pos, vel and t

    return [vel, acc]

y0 = [pos0, vel0]

t = np.linspace(a, b, n)

values = scp.integrate.odeint(edo, y0, t)
position = values[:,0] # first column
velocity = values[:,1] # second column
```

## Shapely
### Interpolation for intersections:
```python
from shapely.geometry import LineString

# If we want the intersection of two functions, whether or not the intersection exists within the dataset (y1 and y2 being 2 different functions)

interpolated_y1 = LineString(np.column_stack((x, y1)))
interpolated_y2 = LineString(np.column_stack((t, y2)))

intersect = interpolated_y1.intersection(interpolated_y2).xy
```

## Matplotlib
### Custom plotting:
```python
import matplotlib.pyplot as plt

fig, ax = plt.subplots(2, 2) # 1, (1,2), (3,3), etc

# Plots [0,1] (top right) and [1,0] (bottom left) are ommited because not necessary for this purpose
# Plot [0,0] (top left)
ax[0,0].plot(x, y, 
        linestyle = "none", # No line
        marker = "x", # Scatter plot with "x" for markers
        color = "red")

ax[0,0].grid()

ax[0,0].set_xlim(a1)
ax[0,0].set_ylim(b1)

ax[0,0].set_xlabel("asd1")
ax[0,0].set_ylabel("asd1")

# Plot [1,1] (bottom right)
ax[1,1].plot(x, y, 
        linestyle = "none", 
        marker = "0", # Scatter plot with round dots
        color = "red")

ax[1,1].grid()

ax[1,1].set_xlim(a2)
ax[1,1].set_ylim(b2)

ax[1,1].set_xlabel("asd2")
ax[1,1].set_ylabel("asd2")
```
### 3 variables on 2D  plot (imshow):
```python
x, y = np.linspace(a1, b1, n), np.linspace(a2, b2, n)
X, Y = np.meshgrid(x, y[::-1])

F = f(X, Y)

plt.figure()

plt.imshow(F, cmap = "rainbow",
           extent = [xi,xf,yi,yf], # i = initial | f = final
           aspect = "auto") # this ensures the image is square | "equal" (aspect ratio = 1)
plt.colorbar()
```

### Polar plotting:
```python
r, theta = np.linspace(0, 2, 100), np.linspace(o, 2*np.pi, 100)

fig, ax = plt.subplots(subplot_kw = {"projection" : "polar"}) # kw stands for keyword

ax.plot(theta, r)

ax.set_rmax(2) # r goes from 0 to 2
ax.set_rticks([0.5, 1, 1.5, 2]) # amount of grid lines (ticks)
ax.set_rlabel_position(-20) # move radial label from plotted line
ax.grid()

ax.set_title("Title")
```
